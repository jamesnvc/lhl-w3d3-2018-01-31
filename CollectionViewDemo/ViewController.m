//
//  ViewController.m
//  CollectionViewDemo
//
//  Created by James Cash on 31-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "OurHeaderView.h"

@interface ViewController () <UICollectionViewDataSource>

@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) IBOutlet UICollectionViewFlowLayout *defaultLayout;

@property (nonatomic,strong) UICollectionViewFlowLayout *bigLayout;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.bigLayout = [[UICollectionViewFlowLayout alloc] init];
    self.bigLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.bigLayout.itemSize = CGSizeMake(300, 300);
    self.bigLayout.minimumInteritemSpacing = 20;
    self.bigLayout.minimumLineSpacing = 50;
    self.bigLayout.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15);
    self.bigLayout.headerReferenceSize = CGSizeMake(0, 150);
    self.bigLayout.footerReferenceSize = CGSizeMake(0, 100);

//    self.collectionView.collectionViewLayout = self.bigLayout;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout changes

- (IBAction)switchLayout:(id)sender {
    UICollectionViewLayout* old = self.collectionView.collectionViewLayout;
    UICollectionViewLayout* new;
    if (old == self.bigLayout) {
        new = self.defaultLayout;
    } else {
        new = self.bigLayout;
    }
    [new invalidateLayout];
    [self.collectionView setCollectionViewLayout:new animated:YES];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 4;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return section + 15;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TheCell" forIndexPath:indexPath];

    UILabel *label = [cell viewWithTag:1];
    label.text = [NSString stringWithFormat:@"%ld, %ld", indexPath.section, indexPath.item];

    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        OurHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"TheHeader" forIndexPath:indexPath];
        header.headerLabel.text = [NSString stringWithFormat:@"Section %ld", indexPath.section];
        return header;
    } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"TheFooter" forIndexPath:indexPath];
        return footer;

    }
    return nil;
}

@end
