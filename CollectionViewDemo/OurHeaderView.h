//
//  OurHeaderView.h
//  CollectionViewDemo
//
//  Created by James Cash on 31-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OurHeaderView : UICollectionReusableView

@property (nonatomic,weak) IBOutlet UILabel *headerLabel;

@end
